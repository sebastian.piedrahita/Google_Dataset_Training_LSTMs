require 'rnn'
require 'nn'
require 'optim'
require 'math'
require 'gnuplot'

date=os.date('%Y_%m_%d_%X')
exp_id = string.format('%s_%s', 'experiment', date)
log_id= exp_id .. '.log'
logger=optim.Logger(log_id)
counter=0 --variable to save epochs
--functions
function save(model, exp_id, numEpochs)
   local filename = 'm.model'
   print('Saving model as ' .. filename)
   torch.save(filename, model)
   local filename = 'model.' .. numEpochs
   print('Saving model as ' .. filename)
   torch.save(filename, model)
end

function save_best_model(model, exp_id, numEpochs)
   --local filename = exp_id .. '.model.' .. numEpochs
   local filename = 'model.best'
   print('Saving model as ' .. filename)
   torch.save(filename, model)
end

function build_network(inputSize, hiddenSize, outputSize)
  
    if (use_saved==0) then
      --Forward LSTM    
      rnn = nn.Sequential() 
      :add(nn.Sequencer(nn.FastLSTM(inputSize, hiddenSize)))
      :add(nn.Sequencer(nn.FastLSTM(hiddenSize, hiddenSize)))
      :add(nn.Sequencer(nn.Linear(hiddenSize, hiddenSize)))
      :add(nn.Sequencer(nn.Sigmoid()))
      :add(nn.Sequencer(nn.Linear(hiddenSize, outputSize)))
      :add(nn.SelectTable(-1))
    else
      rnn=torch.load('m.model')
    end
   return rnn
end





---------------------------------- Scripting -----------------------------------------------------------------
-- Input Matrices
inputs_train= torch.Tensor(size_dataset_train,maximum_length,input_length)
inputs_val= torch.Tensor(size_dataset_val,maximum_length,input_length)
inputs_test= torch.Tensor(size_dataset_test,maximum_length,input_length)

inputs_train= torch.load('../data/inputs_train')
inputs_val=torch.load('../data/inputs_val')
inputs_test=torch.load('../data/inputs_test')
mask_train=torch.load('../data/mask_train')
mask_val=torch.load('../data/mask_val')
mask_test=torch.load('../data/mask_test')
labels_train=torch.load('../data/labels_train')
labels_val=torch.load('../data/labels_val')
labels_test=torch.load('../data/labels_test')


size_dataset_train=37000
size_dataset_val=2500
size_dataset_test=2500
inputs_val=inputs_train[{{39501,42000},{},{}}]
mask_val=mask_train[{{39501,42000}}]
labels_val=labels_train[{{39501,42000}}]
inputs_train = inputs_train[{{1,37000},{},{}}]
mask_train=mask_train[{{1,37000}}]
labels_train=labels_train[{{1,37000}}]

-------------------------------------------------

weighted_cost=torch.Tensor({1,10})
seqC =  nn.CrossEntropyCriterion(weighted_cost)
seqC.nll.sizeAverage = false

expectedTarget={}

local rnn=build_network(number_of_inputs,number_of_neurons,2)
parameters, gradParameters = rnn:getParameters()
light_rnn=rnn:clone('weight','bias','running_mean','running_std')


start = torch.tic()

rnn:training()  
error_vector=torch.Tensor(maxEpochs+1)

best_f1=0;
for numEpochs=0,maxEpochs do
  err_sum=0
  for seqNum=1,size_dataset_train do
    rnn:zeroGradParameters()
    inputSequence={}
    expectedTarget={}
    seqLength=mask_train[seqNum][1]
    
    
      if (labels_train[seqNum]==keyword) then
	  label_temp=2
      else
	  label_temp=1
      end
      
     for j=1,seqLength do
	   table.insert(inputSequence, inputs_train[seqNum][j])
	  
     end
	
     expectedTarget=label_temp
        out = rnn:forward(inputSequence)
	
	  --print('This is a command number: ',expectedTarget )
	  --print(out)
        err = seqC:forward(out, expectedTarget)
        err_sum=err_sum+err     
        gradOut = seqC:backward(out, expectedTarget)
        rnn:backward(inputSequence, gradOut)
        rnn:updateParameters(lr)
        
	--print ('Epoch #', numEpochs .. 'Utterance #', seqNum .. ' of ', dsSize .. '. Loss: ', err)
        logger:add{numEpochs,seqNum,err}
   end
      
   -- at the end of the epoch saving the model obtained:
      local currT=torch.toc(start)
      average_loss=err_sum/dsSize
      --print('Epoch #', numEpochs .. ' average loss', average_loss .. 'in ', currT .. ' s')
      logger:setNames{'Epoch', 'average_loss', 'time', 'learning_rate'}
      logger:add{numEpochs, average_loss, currT,lr}
      save(light_rnn,exp_id,numEpochs)
      
        -- Testing on the Validation Set
      print('Model is now tested on the train set...')
      dofile('/users/micas/giraldo/lstm_google_dataset_torch/scripts/accuracy_over_training_dataset.lua')
      print('Model is now tested on the validation set...')
      counter=numEpochs
      dofile('/users/micas/giraldo/lstm_google_dataset_torch/scripts/validation_stage.lua')
	
	if (f1 > best_f1) then
		best_f1 = f1
		save_best_model(light_rnn,exp_id,numEpochs);	
		
	end	
		
	    --Adaptive learning rate
	   if numEpochs ~= 0 then
		  if average_loss>=previous_average_loss then
		      lr=lr/2
		  else
		  lr=lr*1.01
		  end
	      else
	      previous_average_loss=average_loss
	      end
	      previous_average_loss=average_loss    
	      
	      error_vector[numEpochs+1]= average_loss
  end
  
 

