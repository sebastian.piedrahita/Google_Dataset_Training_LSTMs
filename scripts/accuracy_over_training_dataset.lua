require 'rnn'
require 'optim'
--local matio = require 'matio'

local logger=optim.Logger('training_results.log',1) --timestamp=1
local rnn=torch.load('m.model')   -- Load previously saved nn model
logger:setNames{'Benchmarking on the train set'}
logger:setNames{'seqNum', 'PER'}
rnn:evaluate() -- set rnn to evaluation model

-- Performance variables
        local true_positives=0
	local true_negatives=0
	local true_positives_correctly_classified=0
	local true_negatives_correctly_classified=0
	local classified_as_positives=0
	local fp=0
	local tp=0
	local positive_detected=0
	local positive_expected=0
	
for seqNum=1,size_dataset_train do
     
      inputSequence = {}
      expectedTarget = {}
      local max={}
        --find out the sequence length of each sequence
       mask=mask_train[seqNum][1]
       seqLength=mask
        
       if (labels_train[seqNum] ==keyword) then
	expectedTarget=2
       else
	 expectedTarget=1
       end
       
       
      -- print("Expected Target: " ,expectedTarget)
     for j=1,seqLength do
	table.insert(inputSequence, inputs_train[seqNum][j])
     end
    
        
        --rnn:clearState()
        start =torch.tic() 
        local out = rnn:forward(inputSequence)
        
	--print("Predicted: ", out)
        currT = torch.toc(start)
        --print('Forward pass ' .. seqNum .. ' complete in: '.. currT .. 's')
        
	local predictedTarget={}
	
	-- define all the outputs
	a,predictedTarget = torch.max(out,1)
	
	
	  if expectedTarget>1 then
		  positive_expected=1
	   else
		  positive_expected=0
	    end
	
	 if predictedTarget[1] > 1 then
	      positive_detected=1
	 else
	      positive_detected=0
	 end
	    
		  -- Calculate variables for each sentence
		      if positive_expected==0 then
			true_negatives=true_negatives+1
		      else
			true_positives=true_positives+1
		      end
	
		    if positive_expected==0 then
			if positive_detected == 0 then
			   true_negatives_correctly_classified = true_negatives_correctly_classified+1
			end
		    else
			if positive_detected==1 then
			  true_positives_correctly_classified=true_positives_correctly_classified+1
			end
		    end
		    
		    if positive_detected==1 then
			classified_as_positives=classified_as_positives+1
		    end
		    
		      positive_detected=0;
		      positive_expected=0;
        end
	     
	    
	   
	
	
	
	
	
        

  per_acc= (true_negatives_correctly_classified+true_positives_correctly_classified)/dsSize
  tp= true_positives_correctly_classified/true_positives
  fp=1-(true_negatives_correctly_classified/true_negatives)
  precision= true_positives_correctly_classified/classified_as_positives
  recall= true_positives_correctly_classified/true_positives
  f1= 2/((1/precision)+(1/recall))
  
  
print('True Positives: '.. true_positives)
print('True Negatives: '.. true_negatives)
print('True_positives_correctly_classified: ', true_positives_correctly_classified)
print('True_negatives_correctly_classified: ', true_negatives_correctly_classified)

print('Average PER:' .. per_acc)
logger:setNames{'Epoch', 'Average_PER'}
logger:add{counter, per_acc}
logger:setNames{'============'}

print('Average F1:' .. f1)
logger:setNames{'Epoch', 'F1'}
logger:add{counter, f1}
logger:setNames{'============'}

logger:setNames{'Epoch', 'True Positives:'}
logger:add{counter, true_positives}
logger:setNames{'============'}
logger:setNames{'Epoch', 'True_Negatives'}
logger:add{counter, true_negatives}
logger:setNames{'============'}

logger:setNames{'Epoch', 'True_Positives_correctly_classified'}
logger:add{counter, true_positives_correctly_classified}
logger:setNames{'============'}
logger:setNames{'Epoch', 'True_Negatives_correctly_classified'}
logger:add{counter, true_negatives_correctly_classified}
logger:setNames{'============'}

logger:setNames{'Epoch', 'Classified as positives'}
logger:add{counter, classified_as_positives}
logger:setNames{'============'}

logger:setNames{'Epoch', 'Average Loss'}
logger:add{counter, average_loss}
logger:setNames{'============'}