require 'torch'
require 'hdf5'
require 'io'
-- LOADING DATA FROM CSV TO TENSOR


path_hd5_file='/users/micas/giraldo/nxp_sebastian/training_lstm_kws/data/timit_mfcc_kws_water_TIMIT.h5'
local f = hdf5.open(path_hd5_file, 'r')
  print ('Loading the training set...')
  training_targets=           f:read('/reduced/training/targets'):all()
  training_masks=             f:read('/reduced/training/masks'):all()
  training_labels_reduced=    f:read('/reduced/training/labels_reduced'):all()
  training_labels=            f:read('/reduced/training/labels'):all()
  training_feats=             f:read('/reduced/training/default'):all()
--end

f:close()


local f = hdf5.open(path_hd5_file, 'r')
  print ('Loading the validation set...')
  validation_targets=           f:read('/reduced/validation/targets'):all()
  validation_masks=             f:read('/reduced/validation/masks'):all()
  validation_labels_reduced=    f:read('/reduced/validation/labels_reduced'):all()
  validation_labels=            f:read('/reduced/validation/labels'):all()
  validation_feats=             f:read('/reduced/validation/default'):all()
--end

f:close()


 local f = hdf5.open(path_hd5_file, 'r')
  print ('Loading the test set...')
  test_targets=           f:read('/reduced/test/targets'):all()
  test_masks=             f:read('/reduced/test/masks'):all()
  test_labels_reduced=    f:read('/reduced/test/labels_reduced'):all()
  test_labels=            f:read('/reduced/test/labels'):all()
  test_feats=             f:read('/reduced/test/default'):all()
--end
f:close()

feats_train=training_feats
labels_train=training_targets
masks_train=training_masks

feats_val=validation_feats
labels_val=validation_targets
masks_val=validation_masks

feats_test=test_feats
labels_test=test_targets
masks_test=test_masks

--feats_train=training_feats:permute(2,1,3)
--labels_train=training_targets:permute(2,1,3)
--masks_train=training_masks:permute(2,1,3)

--feats_val=validation_feats:permute(2,1,3)
--labels_val=validation_targets:permute(2,1,3)
--masks_val=validation_masks:permute(2,1,3)


--feats_test=test_feats:permute(2,1,3)
--labels_test=test_targets:permute(2,1,3)
--masks_test=test_masks:permute(2,1,3)

local new_f = hdf5.open('../data/timit_permute_columns_water.h5', 'w')


new_f:write('/reduced/training/default', feats_train)

new_f:write('/reduced/training/masks', masks_train)

new_f:write('/reduced/training/targets', labels_train)

new_f:write('/reduced/validation/default', feats_val)

new_f:write('/reduced/validation/masks', masks_val)

new_f:write('/reduced/validation/targets', labels_val)

new_f:write('/reduced/test/default', feats_test)

new_f:write('/reduced/test/masks', masks_test)

new_f:write('/reduced/test/targets', labels_test)

new_f:close()

print("Assignment")









