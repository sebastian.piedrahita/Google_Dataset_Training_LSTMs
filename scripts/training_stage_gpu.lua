require 'rnn'
require 'optim'
require 'math'
require 'gnuplot'
require 'cutorch'
--if use_cuda then
   -- require 'cutorch'
   -- require 'cunn'
 -- print("Cutorch not loaded")
--end

date=os.date('%Y_%m_%d_%X')
exp_id = string.format('%s_%s', 'experiment', date)
log_id= exp_id .. '.log'
logger=optim.Logger(log_id)

--functions
function save(model, exp_id, numEpochs)
   local filename = 'm.model'
   print('Saving model as ' .. filename)
   torch.save(filename, model)
   local filename = 'model.' .. numEpochs
   print('Saving model as ' .. filename)
   torch.save(filename, model)
end

function save_best_model(model, exp_id, numEpochs)
   --local filename = exp_id .. '.model.' .. numEpochs
   local filename = 'm.best_model'
   print('Saving model as ' .. filename)
   torch.save(filename, model)
end

function build_network(inputSize, hiddenSize, outputSize)
  
    if (use_saved==0) then
      --Forward LSTM    
      rnn = nn.Sequential() 
      :add(nn.Sequencer(nn.FastLSTM(inputSize, hiddenSize)))
      :add(nn.Sequencer(nn.Linear(hiddenSize, hiddenSize)))
      :add(nn.Sequencer(nn.Sigmoid()))
      :add(nn.Sequencer(nn.Linear(hiddenSize, outputSize)))
      :add(nn.SelectTable(-1))
    else
      rnn=torch.load('m.model')
    end
	
	if use_cuda then rnn:cuda() end
	return rnn
end

-- Input Matrices
print("Input vectors are being loaded")
inputs_train= torch.Tensor(size_dataset_train,maximum_length,input_length)
inputs_val= torch.Tensor(size_dataset_val,maximum_length,input_length)
inputs_test= torch.Tensor(size_dataset_test,maximum_length,input_length)

inputs_train= torch.load('../data/inputs_train')
inputs_val=torch.load('../data/inputs_val')
inputs_test=torch.load('../data/inputs_test')
mask_train=torch.load('../data/mask_train')
mask_val=torch.load('../data/mask_val')
mask_test=torch.load('../data/mask_test')
labels_train=torch.load('../data/labels_train')
labels_val=torch.load('../data/labels_val')
labels_test=torch.load('../data/labels_test')


size_dataset_train=37000
size_dataset_val=2500
size_dataset_test=2500
inputs_val=inputs_train[{{39501,42000},{},{}}]
mask_val=mask_train[{{39501,42000}}]
labels_val=labels_train[{{39501,42000}}]
inputs_train = inputs_train[{{1,37000},{},{}}]
mask_train=mask_train[{{1,37000}}]
labels_train=labels_train[{{1,37000}}]

-------------------------------------------------

inputSequence={}
expectedTarget={}


inputSequence_batch_temp=torch.Tensor(batchSize,maximum_length,39)
inputSequence_batch_temp:cuda()
expectedTarget_batch_temp=torch.Tensor(batchSize,maximum_length,1)
---------------------------------- Scripting -----------------------------------------------------------------


build_network(number_of_inputs,number_of_neurons,2)

err_sum=0

print("Now let's go to Train!")
for numEpochs=0,maxEpochs do
  
  for seqNum=1,dsSize-batchSize,batchSize do
	rnn:zeroGradParameters()
	
	for i=0,batchSize-1 do
	    inputSequence_batch_temp[i+1]=inputs_train[seqNum+i]
	    expectedTarget_batch_temp[i+1]=labels_train[seqNum+i]
	end
	inputSequence_batch=inputSequence_batch_temp:permute(2,1,3)  
	inputSequence_batch=inputSequence_batch:cuda()
	expectedTarget_batch=expectedTarget_batch_temp:permute(2,1,3)
	
        out = rnn:forward(inputSequence_batch)
        err = seqC:forward(out, expectedTarget_batch)
        err_sum=err_sum+err     
        gradOut = seqC:backward(out, expectedTarget_batch)
        rnn:backward(inputSequence_batch, gradOut)
        rnn:updateParameters(lr)
        
	print ('Epoch #', numEpochs .. 'Utterance #', seqNum .. ' of ', dsSize .. '. Loss: ', err)
        logger:add{numEpochs,seqNum,err}
   end
	    
  --end
  
   
        -- Testing on the Validation Set
      print('Model is now tested on the train set...')
      dofile('/users/micas/giraldo/lstm_google_dataset_torch/scripts/accuracy_over_training_dataset.lua')
      print('Model is now tested on the validation set...')
      counter=numEpochs
      dofile('/users/micas/giraldo/lstm_google_dataset_torch/scripts/validation_stage.lua')
	
	if (f1 > best_f1) then
		best_f1 = f1
		save_best_model(light_rnn,exp_id,numEpochs);	
		
	end	
   if numEpochs ~= 0 then
    if average_loss>=previous_average_loss then
      lr=lr/2
    else
      lr=lr*1.01
    end
  else
    previous_average_loss=average_loss
  end
    previous_average_loss=average_loss
  
  
end

print("Training has finished")
