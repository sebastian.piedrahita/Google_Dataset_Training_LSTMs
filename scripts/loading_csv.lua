
-- LOADING DATA FROM CSV TO TENSOR



---------------------------------------------------------------------------------------------------------------------
-- Input Matrices
inputs_train= torch.Tensor(size_dataset_train,maximum_length,input_length)
inputs_val= torch.Tensor(size_dataset_val,maximum_length,input_length)
inputs_test= torch.Tensor(size_dataset_test,maximum_length,input_length)




-----LOADING TRAINING DATA  --------------------------------

csvfeatures=io.open(fea_csv_train,'r')
csvmask = io.open(mask_csv_train,'r')
size_dataset=size_dataset_train

data=torch.Tensor(size_dataset*maximum_length,input_length)
mask= torch.Tensor(size_dataset,1)
--mask vector
 i=0
for line in csvmask:lines('*l') do  
  i = i + 1
  local l = line:split(',')
  for key, val in ipairs(l) do
    mask[i][key] = val
  end
  
  if (i==size_dataset) then
    break
  end
end
 csvmask:close()
 
 print("mask loaded training")
 
-- data matrix 
i=0
for line in csvfeatures:lines('*l') do  
  i = i + 1
  local l = line:split(',')

  for key, val in ipairs(l) do
  
    data[i][key] = val
  end
  
    if (i==size_dataset*maximum_length) then
    break
  end
end
 csvfeatures:close()

  print("features loaded training")
-- transforming format to original torch format
 

 i=1
 j=1
 k=1
 current_row=1
 
while i<=size_dataset
do 
     for k=1,input_length do
	inputs_train[i][j][k]= data[current_row][k]
     end
      if (current_row%maximum_length==0) then
	i=i+1
	j=1
      else
	j=j+1
      end
      current_row=current_row+1

end

  mask_train=mask:clone()
  

  
-- LOADING VALIDATION DATA
csvfeatures=io.open(fea_csv_val,'r')
csvmask = io.open(mask_csv_val,'r')
size_dataset=size_dataset_val
----------------------------------
data=torch.Tensor(size_dataset*maximum_length,input_length)
mask= torch.Tensor(size_dataset,1)
--mask vector
 i=0
for line in csvmask:lines('*l') do  
  i = i + 1
  local l = line:split(',')
  for key, val in ipairs(l) do
    mask[i][key] = val
  end
  
    if (i==size_dataset) then
    break
  end
end
 csvmask:close()
 
 print("mask loaded val")
 
-- data matrix 
i=0
for line in csvfeatures:lines('*l') do  
  i = i + 1
  local l = line:split(',')
  for key, val in ipairs(l) do
    data[i][key] = val
  end
  
    if (i==size_dataset*maximum_length) then
    break
  end
end
 csvfeatures:close()

  print("features loaded val")
-- transforming format to original torch format
 i=1
 j=1
 current_row=1
 
while i<=size_dataset
do 
      for k=1,input_length do
	inputs_val[i][j][k]= data[current_row][k]
     end
      if (current_row%maximum_length==0) then
	i=i+1
	j=1
      else
	j=j+1
      end
      current_row=current_row+1
end
 mask_val=mask:clone()
 
 
 
 
-- LOADING TEST DATA 
csvfeatures=io.open(fea_csv_test,'r')
csvmask = io.open(mask_csv_test,'r')
size_dataset=size_dataset_test
----------------------------------
data=torch.Tensor(size_dataset*maximum_length,input_length)
mask= torch.Tensor(size_dataset,1)
--mask vector
 i=0
for line in csvmask:lines('*l') do  
  i = i + 1
  local l = line:split(',')
  for key, val in ipairs(l) do
    mask[i][key] = val
  end
  
    if (i==size_dataset) then
    break
  end
end
 csvmask:close()
 
 print("mask loaded test")
 
-- data matrix 
i=0
for line in csvfeatures:lines('*l') do  
  i = i + 1
  local l = line:split(',')
  for key, val in ipairs(l) do
    data[i][key] = val
  end
  
    if (i==size_dataset*maximum_length)  then
    break
  end
end
 csvfeatures:close()

  print("features loaded test")
-- transforming format to original torch format
 


 i=1
 j=1
 current_row=1
 
while i<=size_dataset
do 
      for k=1,input_length do
	inputs_test[i][j][k]= data[current_row][k]
     end
      if (current_row%maximum_length==0) then
	i=i+1
	j=1
      else
	j=j+1
      end
      current_row=current_row+1
end
  
   mask_test=mask:clone()
   
   
   
   
---- Normalize Input Matrices -----------------------------------------------------
 mean_vector=torch.Tensor(39)
std_vector=torch.Tensor(39)
 
for i=1,39 do
   mean_vector[i] = torch.mean(inputs_test[{{},{{1,20}},i}])
   std_vector[i]= torch.std(inputs_test[{{},{1,20},i}])
   
   inputs_train[{{},{},i}]=(inputs_train[{{},{},i}]-mean_vector[i])/std_vector[i]
  inputs_val[{{},{},i}]=(inputs_val[{{},{},i}]-mean_vector[i])/std_vector[i]
   inputs_test[{{},{},i}]=(inputs_test[{{},{},i}]-mean_vector[i])/std_vector[i]
 end




torch.save('../data/inputs_train',inputs_train)
torch.save('../data/inputs_val',inputs_val)
torch.save('../data/inputs_test',inputs_test)
torch.save('../data/mask_train',mask_train)
torch.save('../data/mask_val',mask_val)
torch.save('../data/mask_test',mask_test)



---------------------------------------------------------------------------------------------------------------------

