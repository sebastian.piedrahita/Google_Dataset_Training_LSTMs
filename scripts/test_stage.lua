require 'rnn'
require 'optim'
--require 'nn'
--require 'util'
--require 'math'
--local matio = require 'matio'

-- Input Matrices
inputs_train= torch.Tensor(size_dataset_train,maximum_length,input_length)
inputs_val= torch.Tensor(size_dataset_val,maximum_length,input_length)
inputs_test= torch.Tensor(size_dataset_test,maximum_length,input_length)

inputs_train= torch.load('../data/inputs_train')
inputs_val=torch.load('../data/inputs_val')
inputs_test=torch.load('../data/inputs_test')
mask_train=torch.load('../data/mask_train')
mask_val=torch.load('../data/mask_val')
mask_test=torch.load('../data/mask_test')
labels_train=torch.load('../data/labels_train')
labels_val=torch.load('../data/labels_val')
labels_test=torch.load('../data/labels_test')


size_dataset_train=37000
size_dataset_val=2500
size_dataset_test=2500
-- Val
inputs_val=inputs_train[{{39501,42000},{},{}}]
mask_val=mask_train[{{39501,42000}}]
labels_val=labels_train[{{39501,42000}}]
-- Test
inputs_test=inputs_train[{{37001,39500},{},{}}]
mask_test=mask_train[{{37001,39500}}]
labels_test=labels_train[{{37001,39500}}]
-- Train
inputs_train = inputs_train[{{1,37000},{},{}}]
mask_train=mask_train[{{1,37000}}]
labels_train=labels_train[{{1,37000}}]





local logger=optim.Logger('test_results.log',1) --timestamp=1
local rnn=torch.load('model.best')   -- Load previously saved nn model
logger:setNames{'Benchmarking on the test set'}
logger:setNames{'seqNum', 'PER'}
rnn:evaluate() -- set rnn to evaluation model

-- Performance variables
        local true_positives=0
	local true_negatives=0
	local true_positives_correctly_classified=0
	local true_negatives_correctly_classified=0
	local classified_as_positives=0
	local fp=0
	local tp=0
	local positive_detected=0
	local positive_expected=0
	

	
for seqNum=1,size_dataset_test do
     
      inputSequence = {}
      expectedTarget = {}
      local max={}
        --find out the sequence length of each sequence
       mask=mask_test[seqNum][1]
       seqLength=mask
        
       if (labels_test[seqNum] ==keyword) then
	expectedTarget=2
       else
	 expectedTarget=1
       end
       
       
       print("Expected Target: " ,expectedTarget)
     for j=1,seqLength do
	table.insert(inputSequence, inputs_test[seqNum][j])
     end
    
        
        --rnn:clearState()
        start =torch.tic() 
        local out = rnn:forward(inputSequence)
        

	print("Predicted: ", out)
        currT = torch.toc(start)
        print('Forward pass ' .. seqNum .. ' complete in: '.. currT .. 's')
        
	local predictedTarget={}
	
	-- define all the outputs
	a,predictedTarget = torch.max(out,1)
	
	
	  if expectedTarget>1 then
		  positive_expected=1
	   else
		  positive_expected=0
	    end
	
	 if predictedTarget[1] > 1 then
	      positive_detected=1
	 else
	      positive_detected=0
	 end
	    
		  -- Calculate variables for each sentence
		      if positive_expected==0 then
			true_negatives=true_negatives+1
		      else
			true_positives=true_positives+1
		      end
	
		    if positive_expected==0 then
			if positive_detected == 0 then
			   true_negatives_correctly_classified = true_negatives_correctly_classified+1
			end
		    else
			if positive_detected==1 then
			  true_positives_correctly_classified=true_positives_correctly_classified+1
			end
		    end
		    
		    if positive_detected==1 then
			classified_as_positives=classified_as_positives+1
		    end
		    
		      positive_detected=0;
		      positive_expected=0;
        end
	     
	    
	   
	
	
	
	
	
        

  per_acc= (true_negatives_correctly_classified+true_positives_correctly_classified)/dsSize
  tp= true_positives_correctly_classified/true_positives
  fp=1-(true_negatives_correctly_classified/true_negatives)
  precision= true_positives_correctly_classified/classified_as_positives
  recall= true_positives_correctly_classified/true_positives
  f1= 2/((1/precision)+(1/recall))
  
  
print('True Positives: '.. true_positives)
print('True Negatives: '.. true_negatives)
print('True_positives_correctly_classified: ', true_positives_correctly_classified)
print('True_negatives_correctly_classified: ', true_negatives_correctly_classified)

print('Average PER:' .. per_acc)
logger:setNames{'Epoch', 'Average_PER'}
logger:add{counter, per_acc}
logger:setNames{'============'}

print('Average F1:' .. f1)
logger:setNames{'Epoch', 'F1'}
logger:add{counter, f1}
logger:setNames{'============'}

logger:setNames{'Epoch', 'True Positives:'}
logger:add{counter, true_positives}
logger:setNames{'============'}
logger:setNames{'Epoch', 'True_Negatives'}
logger:add{counter, true_negatives}
logger:setNames{'============'}

logger:setNames{'Epoch', 'True_Positives_correctly_classified'}
logger:add{counter, true_positives_correctly_classified}
logger:setNames{'============'}
logger:setNames{'Epoch', 'True_Negatives_correctly_classified'}
logger:add{counter, true_negatives_correctly_classified}
logger:setNames{'============'}

logger:setNames{'Epoch', 'Classified as positives'}
logger:add{counter, classified_as_positives}
logger:setNames{'============'}

logger:setNames{'Epoch', 'Average Loss'}
logger:add{counter, average_loss}
logger:setNames{'============'}

