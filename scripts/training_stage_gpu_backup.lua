require 'rnn'
require 'optim'
require 'math'
require 'gnuplot'

date=os.date('%Y_%m_%d_%X')
exp_id = string.format('%s_%s', 'experiment', date)
log_id= exp_id .. '.log'
logger=optim.Logger(log_id)

--functions
function save(model, exp_id, numEpochs)
   local filename = 'm.model'
   print('Saving model as ' .. filename)
   torch.save(filename, model)
   local filename = 'model.' .. numEpochs
   print('Saving model as ' .. filename)
   torch.save(filename, model)
end

function save_best_model(model, exp_id, numEpochs)
   --local filename = exp_id .. '.model.' .. numEpochs
   local filename = 'm.best_model'
   print('Saving model as ' .. filename)
   torch.save(filename, model)
end

function build_network(inputSize, hiddenSize, outputSize)
 
      --Forward LSTM    
      rnn = nn.Sequential() 
      :add(nn.Sequencer(nn.FastLSTM(inputSize, hiddenSize)))
      :add(nn.SelectTable(-1))
      :add(nn.Sequencer(nn.Linear(hiddenSize, outputSize)))
      if use_cuda then rnn:cuda() end
      

   return rnn
end

inputSequence={}
expectedTarget={}


inputSequence_batch_temp=torch.Tensor(batchSize,maxseqLength,39)
inputSequence_batch_temp:cuda()
expectedTarget_batch_temp=torch.Tensor(batchSize,maxseqLength,1)
---------------------------------- Scripting -----------------------------------------------------------------


build_network(39,50,2)

err_sum=0
for numEpochs=0,100 do
  
  for seqNum=1,dsSize-batchSize,batchSize do
	rnn:zeroGradParameters()
	
	for i=0,batchSize-1 do
	    inputSequence_batch_temp[i+1]=inputs_train[seqNum+i]
	    expectedTarget_batch_temp[i+1]=labels_train[seqNum+i]
	end
	inputSequence_batch=inputSequence_batch_temp:permute(2,1,3)  
	inputSequence_batch=inputSequence_batch:cuda()
	expectedTarget_batch=expectedTarget_batch_temp:permute(2,1,3)
	
        out = rnn:forward(inputSequence_batch)
        err = seqC:forward(out, expectedTarget_batch)
        err_sum=err_sum+err     
        gradOut = seqC:backward(out, expectedTarget_batch)
        rnn:backward(inputSequence_batch, gradOut)
        rnn:updateParameters(lr)
        
	print ('Epoch #', numEpochs .. 'Utterance #', seqNum .. ' of ', dsSize .. '. Loss: ', err)
        logger:add{numEpochs,seqNum,err}
   end
	    
  end
  
 
   if numEpochs ~= 0 then
    if average_loss>=previous_average_loss then
      lr=lr/2
    else
      lr=lr*1.01
    end
  else
    previous_average_loss=average_loss
  end
    previous_average_loss=average_loss
  
  
end
