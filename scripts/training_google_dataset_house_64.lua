--Parameters
--size_dataset_train=50721
--size_dataset_val=7000
--size_dataset_test=7000

--files
fea_csv_train='/users/micas/giraldo/lstm_google_dataset_torch/data/features_train_GOOGLE.csv'
fea_csv_val='/users/micas/giraldo/lstm_google_dataset_torch/data/features_val_GOOGLE.csv'
fea_csv_test='/users/micas/giraldo/lstm_google_dataset_torch/data/features_test_GOOGLE.csv'
mask_csv_train='/users/micas/giraldo/lstm_google_dataset_torch/data/mask_train_GOOGLE.csv'
mask_csv_val='/users/micas/giraldo/lstm_google_dataset_torch/data/mask_val_GOOGLE.csv'
mask_csv_test='/users/micas/giraldo/lstm_google_dataset_torch/data/mask_test_GOOGLE.csv'

-- Size of input vectors
size_dataset_train=42000 --number of files. The maximum number is 50721
size_dataset_val=2500 --number of files. The maximum number is 7000
size_dataset_test=2500  --number of files.  The maximum number is 7000
dsSize=size_dataset_train
maximum_length=70
input_length=39
number_of_inputs=39
keyword=20 -- There are 30 commands, keyword is the command that we want to detect

--Hyperparameters
lr=0.005--Learning Rate
maxEpochs=1000 --maximum number of epochs to train
number_of_neurons=64


--GPU
use_cuda=1
use_saved=1
batchSize=1


print('Loaded parameters')


dofile('/users/micas/giraldo/lstm_google_dataset_torch/scripts/training_stage.lua')


